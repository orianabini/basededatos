import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Scanner;

public class Ejecutable {

	public static void main(String[] args) throws SQLException, ClassNotFoundException {

		BaseDeDatos bd = new BaseDeDatos();
		Producto p1 = new Producto("Sandia", 30, 300.50);
		bd.agregarProductoALaTabla(p1.getNombre(), p1.getCantidad(), p1.getPrecio());
		bd.updateProductoPorID(3, "Sandia", 100, 1254.59);

		Cliente cl1 = new Cliente("Enzo", "Gomez", "3545655", "Enzogomez@gmail.com", "ayacucho 5000", "Rosario");
		System.out.println("Nombre del cliente: " + cl1.getNombre());

		bd.agregarClienteALaTabla(cl1.getNombre(), cl1.getApellido(), cl1.getTelefono(), cl1.getDireccion(),
				cl1.getDireccion(), cl1.getLocalidad());

		/*
		 * bd.borrarTodosLosRegistrosDeLaTablaProductos();
		 */
	}
}
